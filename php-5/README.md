# PHP 5
## Requirement
This project requires PHP 5.

## How to run
```bash
php hello-world.php
```
This will display a hello world message if executed with PHP 5 and an error message otherwise.

## build and run docker
docker build -t mon_image_php5:latest .
docker run mon_image_php5