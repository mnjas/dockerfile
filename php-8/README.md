# PHP 8
## Requirement
This project requires PHP 8.

## How to run
```bash
php hello-world.php
```
This will display a hello world message if executed with PHP 8 and an error message otherwise.

## build and run docker
docker build -t mon_image_php8:latest .
docker run mon_image_php8