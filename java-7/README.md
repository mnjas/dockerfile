# Java 7
## Requirement
This project requires Java 7.

## How to run
```bash
javac HelloWorld.java
java HelloWorld
```
## build and run docker
docker build -t mon_image_java:latest .
docker run mon_image_java